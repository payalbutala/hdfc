/*
  @Author : Fractalink Design Studio
  @Project : Hdfc 
  @Dev : Irfan Khan
  @Date : 07 July 2015;
*/

/*** Grid Js ***/
function createJsonGrid(jsonURL, grid, colName, colMdl, multiOpt, pagerId, gridVarJson, addFrm) {

    'use strict';
    var applyClassesToHeaders = function(gridObj) {
        // Use the passed in grid as context, 
        // in case we have more than one table on the page.
        var trHead = jQuery("thead:first tr", gridObj.hdiv);
        var colModel = jQuery(gridObj).jqGrid("getGridParam", "colModel");



        for (var iCol = 0; iCol < colModel.length; iCol++) {
            var columnInfo = colModel[iCol];
            if (columnInfo.classes) {
                var headDiv = jQuery("th:eq(" + iCol + ")", trHead);
                headDiv.addClass(columnInfo.classes);
            }
        }
    };
    grid.jqGrid({
        url: jsonURL,
        datatype: "json",
        loadonce: true,
        altclass: "odd",
        altRows: true,
        colNames: colName,
        colModel: colMdl,
        autowidth: false,
        gridview: true,
        rownumbers: false,
        viewrecords: true,
        height: '100%',
        forceFit: false,
        pager: pagerId,
        rowNum: 10,
        pagerpos: 'left',
        multiselect: multiOpt,
        jsonReader: {
            repeatitems: false,
            cifNo: "cifNo",
            root: function(obj) {
                return obj;
            }
        },
        onSelectRow: function(rowid, status) {
            if($('#'+rowid).attr("editable")==1)
            {
                if ($('#'+rowid).attr("aria-selected") == 'false')
                {
                    jQuery(grid).setSelection(rowid, true);
                }
            };

        },
        gridComplete: function() {
            var rowdata = jQuery(grid).jqGrid('getRowData');
            var width = jQuery(grid).width();
            jQuery(grid).setGridWidth(width);
            $(".ui-jqgrid-view").width(width + 2);

            $('.editableTable .ui-jqgrid-bdiv').css({
                "overflow-x": "hidden",
                "overflow-y": "scroll"
            });

            $('.editableTable .ui-jqgrid-bdiv').height(550);

            $(".icn-edit").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var editData = jQuery("#demoGrid").jqGrid('getRowData', id);
                console.log("Edit Row Start")
                console.log(editData)
                console.log("Edit Row End")
                jQuery('#demoGrid').editRow(id);

                // console.log($(this).parents("tr").attr("editable") == "1")
                // console.log($(this).parents("tr").attr("aria-selected"))
                if ($(this).parents("tr").attr("aria-selected") == false || $(this).parents("tr").attr("aria-selected") == undefined)
                    jQuery("#demoGrid").setSelection(id, true);
            })

            $(".icn-del").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var delData = jQuery("#demoGrid").jqGrid('getRowData', id);
                console.log("Delete Row Start")
                console.log(delData)
                console.log("Delete Row End")
                jQuery('#demoGrid').delRowData(id);
            })

        }

    }).navGrid(pagerId, { edit: false, add: false, del: false, search: false, view: false });


     $(".gridContainer").on("click", ".addData", function() {

        var ipLen = $(".custTbl").find("input");

        var dt = {}
        $(ipLen).each(function(){
            dt[$(this).attr("id")]=$(this).val();
        })

        jQuery(grid).jqGrid("addRowData", undefined, dt, "first");

        $(ipLen).each(function(){
            $(this).val('');
        })

    });

    if(addFrm)
    {
        var colLen = gridVarJson.colModel.length;
        var tblEle = $("<table class='table table-striped custom-table custTbl' />")
        var tbRow = $("<tr/>")
        var colGrp = $("<colgroup>")

            for (var i = 0; i < colLen; i++) {
                $(colGrp).append("<col style='width:"+gridVarJson.colModel[i].width+"%'>")
            }
            $(colGrp).prepend("<col style='width:25px'>")
        $(tblEle).append(colGrp)
        for (var i = 0; i < colLen; i++) {
            if (gridVarJson.colModel[i].name != 'edit' && gridVarJson.colModel[i].name != 'del') {
                $(tbRow).append("<td class='tbl-text'><input type='text' id='" + gridVarJson.colModel[i].name + "'/></td>")
            }else{
                 $(tbRow).append("<td class='tbl-text "+gridVarJson.colModel[i].name+"'></td>")
            }
        }
        $(tbRow).prepend("<td>&nbsp;</td>")
        $(tbRow).find("td.edit").html("<span class='icn-add addData'> </span>")
        $(tblEle).append(tbRow);
        $(grid).before(tblEle);
    }

    var colModel1 = grid.jqGrid('getGridParam', 'colModel');
    var sortName = grid.jqGrid('getGridParam', 'sortname');

    $('#gbox_' + $.jgrid.jqID(grid[0].id) +
        ' tr.ui-jqgrid-labels th.ui-th-column').each(function(i) {

        var cmi = colModel1[i],
            colName = cmi.name;

        if (cmi.sortable !== false) {
            $(this).find('>div.ui-jqgrid-sortable>span.s-ico').show();
        } else if (!cmi.sortable && colName !== 'rn' && colName !== 'cb' && colName !== 'subgrid') {
            $(this).find('>div.ui-jqgrid-sortable').css({
                cursor: 'default'
            });
        }
        if (cmi.name === sortName) {
            $(this).addClass('sortedColumnHeader');
        }
    });

    applyClassesToHeaders(grid)
}

function resizeGrid(grid, wdthCont) {
    jQuery(window).bind('resize', function() {

        // Get width of parent container
        var width = jQuery(wdthCont).width();
        //console.log("width:" + width)
        if (width == null || width < 1) {
            // For IE, revert to offsetWidth if necessary
            width = jQuery(wdthCont).width();
        }
        width = width - 2; // Fudge factor to prevent horizontal scrollbars
        if (width > 0 &&
            // Only resize if new width exceeds a minimal threshold
            // Fixes IE issue with in-place resizing when mousing-over frame bars
            Math.abs(width - jQuery(grid).width()) > 5) {
            //$('.ui-jqgrid-bdiv').mCustomScrollbar("destroy")
            // alert(width);
            jQuery(grid).setGridWidth(width);

            $(".ui-jqgrid-view").width(width + 2);

            if ($(grid).attr("id") == "tblImpGrid") {
                $(grid).jqGrid('destroyGroupHeader');
                grid.jqGrid('setGroupHeaders', {
                    useColSpanStyle: false,
                    groupHeaders: [{ startColumnName: 'kpiName', numberOfColumns: 2, titleText: 'KPI' },
                        { startColumnName: 'globalParamName', numberOfColumns: 2, titleText: 'Global Parameter' },
                        { startColumnName: 'localParamName', numberOfColumns: 2, titleText: 'Local Parameter' }
                    ]
                });
            }


        }

    }).trigger('resize');
}

var demoGridVar = {

    colName: ['CIF NO.', 'AMT. TO BE DISBURSED', 'VIRTUAL A/C NO.', 'DEALER UNIQUE CODE', 'TRANCHE ID',
        'EDIT', ''
    ],
    colModel: [
        { name: 'cifNo', index: 'cifNo', width: 12, sortable: false, editable: true },
        { name: 'amtDisbursed', width: 20, sortable: false, classes: "text-green align-text-right", editable: true },
        { name: 'virtAcNo', width: 18, sortable: false, editable: true },
        { name: 'dealercode', width: 20, sortable: false, editable: true },
        { name: 'TranId', width: 15, sortable: false, editable: true }, {
            name: 'edit',
            width: 5,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-edit" href="#">edit</a>';
            }
        }, {
            name: 'del',
            width: 5,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-del" href="#">delete</a>';
            }
        },
    ]
}




/*Document Ready*/
$(function() {

    if ($("#demoGrid").length != 0) {
        var demoGrid = jQuery("#demoGrid")
        createJsonGrid('data/demo.json', demoGrid, demoGridVar.colName, demoGridVar.colModel, true, '#demoPager', demoGridVar, true)
        resizeGrid(demoGrid, ".gridContainer");
    }

    /* Js to retrieve data of selected rows */
    jQuery(".getData").click(function() {
        var getId = jQuery("#demoGrid").jqGrid('getGridParam', 'selarrrow');
        if (getId != "") {
            for (i in getId) {
                var selData = jQuery("#demoGrid").jqGrid('getRowData', getId[i]);
                console.log("Selected Row Start")
                console.log(selData)
                console.log("Selected Row end")
            }
        } else {
            alert("Please select row");
        }
    });

    /* Js to retrieve data of Deleted rows */
    jQuery(".delData").click(function() {
        var delId = jQuery("#demoGrid").jqGrid('getGridParam', 'selarrrow');
        var tt = [];
        if (delId != "") {
            for (var i = 0; i < delId.length; i++) {
                delData = jQuery("#demoGrid").jqGrid('getRowData', delId[i]);
                console.log("Deleted Row Start")
                console.log(delData)
                console.log("Deleted Row End")
                tt.push(delId[i])
            }
            for (obj in tt) {
                jQuery("#demoGrid").jqGrid('delRowData', tt[obj]);
            }
        } else {
            alert("Please select row");
        }
    });

    /* Js to retrieve data of Edited rows */
    jQuery(".editData").click(function() {
        var editId = jQuery("#demoGrid").jqGrid('getGridParam', 'selarrrow');
        if (editId != "") {
            for (id in editId) {
                editData = jQuery("#demoGrid").jqGrid('getRowData', editId[id]);
                console.log("Edit Row Start")
                console.log(editData)
                console.log("Edit Row end")
                jQuery("#demoGrid").jqGrid('editRow', editId[id]);
            }
        } else {
            alert("Please select row");
        }
    });

    //console.log("addData--" + $(".addData").length)

    /* Js to retrieve data of Save rows */
    jQuery(".btn-save").click(function() {
        var rowId = jQuery("#demoGrid").jqGrid('getGridParam', 'selarrrow');
        //console.log("rowId" + rowId)
        if (rowId != "") {
            for (id in rowId) {
                //console.log("id--" + id)
                jQuery("#demoGrid").jqGrid('saveRow', rowId[id]);
                svData = jQuery("#demoGrid").jqGrid('getRowData', rowId[id]);

                console.log("Save Row Start")
                console.log(svData)
                console.log("Save Row End")
            }
        } else {
            alert("Please select row");
        }

    });

    

})
