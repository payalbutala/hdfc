/*
  @Author : Fractalink Design Studio
  @Project : Hdfc 
  @Dev : Irfan Khan
  @Date : 07 July 2015;
*/


/*** CheckBox function :- Start ***/
function chkBoxClick(valChk) {

    var btnChk = $(valChk);

    $(btnChk).each(function() {

        if ($(this).is(':checked') == true) {
            if ($(this).parent().hasClass("inActive") == false) {
                $(this).parents(".checkbox").addClass("checked");
                $(this).parent().addClass("active");
                keypadShowHide();
                custom_tandc();
            }
        } else {
            $(this).parents(".checkbox").removeClass("checked");
            $(this).parent().removeClass("active");
            custom_tandc();
        }
    });

    $(btnChk).change(function() {

        $(btnChk).each(function() {

            if ($(this).is(':checked') == true) {
                if ($(this).parent().hasClass("inActive") == false) {
                    $(this).parents(".checkbox").addClass("checked");
                    $(this).parent().addClass("active");
                    keypadShowHide();
                    custom_tandc();
                } else {
                    $(this).attr('checked', false);
                }
            } else {
                $(this).parents(".checkbox").removeClass("checked");
                $(this).parent().removeClass("active");
                custom_tandc();
            }
        });

    });
}
/*** CheckBox function :- End ***/

function keypadShowHide() {
    if ($("#virtualKeypad").is(':checked') == true) {


        $(".login-cont-wrap").addClass("virtual-keypad-active");
        $("#virtualKeypad").parent().parent().parent().hide();


    }

    $(".close-virtual-keypad").on('click', function() {
        $(".login-cont-wrap").removeClass("virtual-keypad-active");
        $("#virtualKeypad").prop("checked", false);
        $("#virtualKeypad").parents(".checkbox").removeClass("checked");
        $("#virtualKeypad").parent().removeClass("active");
        $("#virtualKeypad").parent().parent().parent().show();
    });

}


function custom_tandc() {
    if ($("#termcondition").is(':checked') == true) {
        var tarUrl = "template_terms_conditions.html";
        $(".tandc").attr("href", "template_terms_conditions.html");
    } else {
        $(".tandc").attr("href", "#");
        return false;
    }
}

function loginBanner() {
    $("#banner-slider").carousel({
        interval: 10000,
        pause: false
    });
}

function customSelect() {
    $("select.form-control").select2();
}

function slideUpDown(obj, objCont, objLink) {
    $(obj).find(objLink).on("click", function() {
        if ($(obj).hasClass("open") == true) {
            $(objCont).slideUp(400);
            $(obj).removeClass("open");
        } else {
            $(objCont).slideDown(400);
            $(obj).addClass("open");
        }
    })
}

function showHide(obj, objCont, objParent) {
    $(objParent).slideUp(400);
    $(obj).on("click", function() {
        if ($(objCont).hasClass("open") == true) {
            $(objParent).slideUp(400);
            $(objCont).removeClass("open");
        } else {
            $(objParent).slideDown(400);
            $(objCont).addClass("open");
        }
    })
}

/*** Date Range Js ***/
function dateRange(startObj, endObj, startOpt, endOpt) {
    $(startObj).datetimepicker(startOpt);

    $(endObj).datetimepicker(endOpt);

    var cDate = new Date();
    var cM = cDate.getMonth();
    var cD = cDate.getDate();
    var cY = cDate.getFullYear();
    var endDate = new Date()
    var newDate = endDate.setDate(cD + 7);
    var eM = endDate.getMonth()
    var eD = endDate.getDate()
    var eY = endDate.getFullYear()

    $(startObj).datetimepicker('update', "'" + cD + "/" + (cM + 1) + "/" + cY + "'");
    $(endObj).datetimepicker('update', "'" + eD + "/" + (eM + 1) + "/" + eY + "'");


    $(startObj)
        .datetimepicker()
        .on('changeDate', function(ev) {

            $(endObj).datetimepicker('setStartDate', ev.date);

        });

    $(endObj)
        .datetimepicker()
        .on('changeDate', function(ev) {

            $(startObj).datetimepicker('setEndDate', ev.date);

        });


}

/*** Grid Js ***/
function createGrid(grid, griddata, colName, colMdl, multiOpt, pagerId, subHeaderOpt, subHeading, griddataVar, editOpt, ftrOpt, dataOnFtrOpt) {
    'use strict';
    var applyClassesToHeaders = function(gridObj) {
        // Use the passed in grid as context, 
        // in case we have more than one table on the page.
        var trHead = jQuery("thead:first tr", gridObj.hdiv);
        var colModel = jQuery(gridObj).jqGrid("getGridParam", "colModel");

        for (var iCol = 0; iCol < colModel.length; iCol++) {
            var columnInfo = colModel[iCol];
            if (columnInfo.classes) {
                var headDiv = jQuery("th:eq(" + iCol + ")", trHead);
                headDiv.addClass(columnInfo.classes);
            }
        }
        //console.log()
        if (subHeaderOpt == true) {
            var grpHeader = subHeading
            for (var iCol = 0; iCol < grpHeader.length; iCol++) {
                var columnInfo = grpHeader[iCol];
                if (columnInfo.classes) {
                    var headDiv = jQuery(trHead[1]).find("th[colspan]").eq(iCol);
                    headDiv.addClass(columnInfo.classes);
                }
            }
        }

    };
    grid.jqGrid({
        datatype: 'local',
        data: griddata,
        altclass: "odd",
        altRows: true,
        colNames: colName,
        colModel: colMdl,
        autowidth: false,
        gridview: true,
        rownumbers: false,
        viewrecords: true,
        height: '100%',
        forceFit: false,
        pager: pagerId,
        rowNum: 10,
        pagerpos: 'left',
        multiselect: multiOpt,
        footerrow: ftrOpt,
        userDataOnFooter: dataOnFtrOpt,
        onSelectRow: function(rowid, status) {
            if ($('#' + rowid).attr("editable") == 1) {
                if ($('#' + rowid).attr("aria-selected") == 'false') {
                    jQuery(grid).setSelection(rowid, true);
                }
            };

        },
        gridComplete: function() {
            var rowdata = jQuery(grid).jqGrid('getRowData');
            var width = jQuery(grid).width();
            jQuery(grid).setGridWidth(width);
            $(".ui-jqgrid-view").width(width + 2);

            for (var i = 0; i < rowdata.length; i++) {
                var validType = griddata[i].validType;
                $(grid).find("#" + (i + 1)).addClass(validType);
                var rowCell = $(grid).find("#" + (i + 1)).find("td");
            }

            if (subHeaderOpt == true) {
                grid.jqGrid('setGroupHeaders', {
                    groupHeaders: subHeading
                });
            }

            if (ftrOpt == true && dataOnFtrOpt == true) {
                for (i = 0; i < griddataVar.colModel.length; i++) {
                    var totalSum = $(grid).jqGrid('getCol', griddataVar.colModel[i].name, false, 'sum');
                    //console.log(griddataVar.colModel[i].name)
                    var dataObj = {}
                    var grid_Obj = jQuery(grid).attr('id');
                    var appEle = "#gbox_" + grid_Obj;
                    //console.log($(appEle).find(".footrow td").eq(i))

                    dataObj[griddataVar.colModel[i].name] = "<span>" + totalSum + "</span>";
                    $(grid).jqGrid('footerData', 'set', dataObj);
                }
            }

            $(pagerId).find(".ui-pg-input").attr("disabled","disabled");

            $('.editableTable .ui-jqgrid-bdiv').css({
                "overflow-x": "hidden",
                "overflow-y": "scroll"
            });

            $('.editableTable .ui-jqgrid-bdiv').height(550);

            $(".icn-edit").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var editData = jQuery(grid).jqGrid('getRowData', id);
                console.log("Edit Row Start")
                console.log(editData)
                console.log("Edit Row End")
                jQuery(grid).editRow(id);

                if ($(this).parents("tr").attr("aria-selected") == false || $(this).parents("tr").attr("aria-selected") == undefined)
                    jQuery(grid).setSelection(id, true);
            })

            $(".icn-del").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var delData = jQuery(grid).jqGrid('getRowData', id);
                console.log("Delete Row Start")
                console.log(delData)
                console.log("Delete Row End")
                jQuery(grid).delRowData(id);
            })

        },
        loadComplete: function() {
            if (editOpt == true) {
                var $this = $(this),
                    ids = $this.jqGrid('getDataIDs'),
                    i, l = ids.length;
                for (i = 0; i < l; i++) {
                    //var row = jQuery(grid).jqGrid ('getRowData', ids[i]);
                    if (griddata[i].cellEditParam) {
                        $this.jqGrid('editRow', ids[i], true);
                    } else {
                        var grid_Obj = jQuery(grid).attr('id');
                        var appEle = "#" + grid_Obj;
                        //$(appEle).find("td").length;
                        console.log(ids[i])
                        $(appEle).find("tr").eq(ids[i]).find("td").each(function(){
                            var v = $(this).text()
                            $(this).html("<span>"+v+"</span>")
                        })
                        //$(appEle).find("tr").eq(ids[i]).find("td").append("<span>'+ +'</span>");
                        // /$(appEle).find("td").append("</span>");
                    }
                }
            }
        }

    }).navGrid(pagerId, { edit: false, add: false, del: false, search: false, view: false });

    /* Js to retrieve data of selected rows */
    jQuery(".getData").click(function() {
        var getId = jQuery(grid).jqGrid('getGridParam', 'selarrrow');
        if (getId != "") {
            for (i in getId) {
                var selData = jQuery(grid).jqGrid('getRowData', getId[i]);
                console.log("Selected Row Start")
                console.log(selData)
                console.log("Selected Row Start")
            }
        } else {
            alert("Please select row");
        }
    });

    /* Js to retrieve data of Deleted rows */
    jQuery(".delData").click(function() {
        var delId = jQuery(grid).jqGrid('getGridParam', 'selarrrow');
        var tt = [];
        if (delId != "") {
            for (var i = 0; i < delId.length; i++) {
                var delData = jQuery(grid).jqGrid('getRowData', delId[i]);
                console.log("Deleted Row Start")
                console.log(delData)
                console.log("Deleted Row End")
                tt.push(delId[i])
            }
            for (obj in tt) {
                jQuery(grid).jqGrid('delRowData', tt[obj]);
            }
        } else {
            alert("Please select row");
        }
    });

    /* Js to retrieve data of Edited rows */
    jQuery(".editData").click(function() {
        var editId = jQuery(grid).jqGrid('getGridParam', 'selarrrow');
        if (editId != "") {
            for (id in editId) {
                var editData = jQuery(grid).jqGrid('getRowData', editId[id]);
                console.log("Edit Row Start")
                console.log(editData)
                console.log("Edit Row Start")
                jQuery(grid).jqGrid('editRow', editId[id]);
            }
        } else {
            alert("Please select row");
        }
    });

    /* Js to retrieve data of Save rows */
    jQuery(".btn-save-data").click(function() {
        var rowId = jQuery(grid).jqGrid('getGridParam', 'selarrrow');
        if (rowId != "") {
            for (var id in rowId) {
                jQuery(grid).jqGrid('saveRow', rowId[id]);
                var svData = jQuery(grid).jqGrid('getRowData', rowId[id]);

                console.log("Save Row Start")
                console.log(svData)
                console.log("Save Row End")
            }
        } else {
            alert("Please select row");
        }

    });


    /* To add columns in submission salesInfoGrid */

    if (jQuery(grid).attr("id") == "salesInfoGrid") {
        console.log("grid" + grid)
        var colLen = griddataVar.colModel.length;
        var tblEle = $("<table class='table table-striped custom-table salesTbl' />")
        var tbRow = $("<tr/>")
        var colGrp = $("<colgroup>")

        for (var i = 0; i < colLen; i++) {
            $(colGrp).append("<col style='width:" + griddataVar.colModel[i].width + "%'>")
        }

        // $(colGrp).prepend("<col style='width:0' >")
        $(tblEle).append(colGrp)
        for (var i = 0; i < colLen; i++) {
            if (griddataVar.colModel[i].name != 'edit' && griddataVar.colModel[i].name != 'del') {
                $(tbRow).append("<td class='tbl-text'><input type='text' id='" + griddataVar.colModel[i].name + "'/></td>")
            } else {
                $(tbRow).append("<td class='tbl-text " + griddataVar.colModel[i].name + "'></td>")
            }
        }
        // $(tbRow).prepend("<td>&nbsp;</td>")
        $(tbRow).find("td.edit").html("<span class='icn-add addData'> </span>")

        var newRow = $("<tr/>");
        $(newRow).append("<td colspan='11' class='actBtnWrap'></td>");
        var struct = "<div class='pull-right'><div class='checkbox pull-left'><label for='termcondition'><input type='checkbox' id='termcondition'> </label><a class='tandc' href='template_terms_conditions.html'>I agree to terms &amp; conditions</a></div><div class='act-btn-block pull-right'> <a href = '#' class = 'act-btn btn-save'>save </a> <a href = '#' class = 'act-btn btn-submit' >submit </a> </div></div>"
        $(newRow).find("td.actBtnWrap").html(struct);
    }

    $(tblEle).append(tbRow);
    $(tblEle).append(newRow);
    $(grid).before(tblEle);

    if ($("#termcondition").length != 0) {
        if ($(".checkbox").length != 0) {
            var chkbtnName = $(".checkbox input[type=checkbox]");
            chkBoxClick(chkbtnName);
        }

    }

    var colModel1 = grid.jqGrid('getGridParam', 'colModel');
    var sortName = grid.jqGrid('getGridParam', 'sortname');

    $('#gbox_' + $.jgrid.jqID(grid[0].id) +
        ' tr.ui-jqgrid-labels th.ui-th-column').each(function(i) {

        var cmi = colModel1[i],
            colName = cmi.name;

        if (cmi.sortable !== false) {
            $(this).find('>div.ui-jqgrid-sortable>span.s-ico').show();
        } else if (!cmi.sortable && colName !== 'rn' && colName !== 'cb' && colName !== 'subgrid') {
            $(this).find('>div.ui-jqgrid-sortable').css({
                cursor: 'default'
            });
        }
        if (cmi.name === sortName) {
            $(this).addClass('sortedColumnHeader');
        }
    });

    applyClassesToHeaders(grid)
}

function createJsonGrid(jsonURL, grid, colName, colMdl, multiOpt, pagerId, gridVarJson, addFrm, chkBoxShowHide) {
    'use strict';
    var applyClassesToHeaders = function(gridObj) {
        // Use the passed in grid as context, 
        // in case we have more than one table on the page.
        var trHead = jQuery("thead:first tr", gridObj.hdiv);
        var colModel = jQuery(gridObj).jqGrid("getGridParam", "colModel");



        for (var iCol = 0; iCol < colModel.length; iCol++) {
            var columnInfo = colModel[iCol];
            if (columnInfo.classes) {
                var headDiv = jQuery("th:eq(" + iCol + ")", trHead);
                headDiv.addClass(columnInfo.classes);
            }
        }
    };
    grid.jqGrid({
        url: jsonURL,
        datatype: "json",
        loadonce: true,
        altclass: "odd",
        altRows: true,
        colNames: colName,
        colModel: colMdl,
        autowidth: false,
        gridview: true,
        rownumbers: false,
        viewrecords: true,
        height: '100%',
        forceFit: false,
        pager: pagerId,
        rowNum: 10,
        pagerpos: 'left',
        multiselect: multiOpt,
        jsonReader: {
            repeatitems: false,
            cifNo: "cifNo",
            root: function(obj) {
                return obj;
            }
        },
        onSelectRow: function(rowid, status) {
            if ($('#' + rowid).attr("editable") == 1) {
                if ($('#' + rowid).attr("aria-selected") == 'false') {
                    jQuery(grid).setSelection(rowid, true);
                }
            };

        },
        loadComplete: function() {
            // we make all even rows "protected", so that will be not selectable

            var grid_Obj = jQuery(grid).attr('id');
            var appEle = "#gbox_" + grid_Obj
            if (chkBoxShowHide == false) {


                $(appEle).find(".cbox").css("opacity", "0").attr("disabled", "disabled");
            }
        },
        gridComplete: function() {
            var rowdata = jQuery(grid).jqGrid('getRowData');
            var width = jQuery(grid).width();
            jQuery(grid).setGridWidth(width);
            $(".ui-jqgrid-view").width(width + 2);

            $('.editableTable .ui-jqgrid-bdiv').css({
                "overflow-x": "hidden",
                "overflow-y": "scroll"
            });

            $('.editableTable .ui-jqgrid-bdiv').height(550);

            $("#modelDetGrid").jqGrid('setSelection', '1');


            $(".icn-edit").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var editData = jQuery(grid).jqGrid('getRowData', id);
                console.log("Edit Row Start")
                console.log(editData)
                console.log("Edit Row End")
                jQuery(grid).editRow(id);

                if ($(this).parents("tr").attr("aria-selected") == false || $(this).parents("tr").attr("aria-selected") == undefined)
                    jQuery(grid).setSelection(id, true);
            })

            $(".icn-del").on("click", function() {
                var id = $(this).parents("tr").attr("id")
                var delData = jQuery(grid).jqGrid('getRowData', id);
                console.log("Delete Row Start")
                console.log(delData)
                console.log("Delete Row End")
                jQuery(grid).delRowData(id);
            })

        }

    }).navGrid(pagerId, { edit: false, add: false, del: false, search: false, view: false });


    /* Js to retrieve data of Save rows */
    jQuery(".btn-save-data").click(function() {
        var rowId = jQuery(grid).jqGrid('getGridParam', 'selarrrow');
        if (rowId != "") {
            for (var id in rowId) {
                jQuery(grid).jqGrid('saveRow', rowId[id]);
                var svData = jQuery(grid).jqGrid('getRowData', rowId[id]);

                console.log("Save Row Start")
                console.log(svData)
                console.log("Save Row End")
            }
        } else {
            alert("Please select row");
        }

    });

    /* Js to retrieve data of Add Blank row */
    $(".gridContainer").on("click", ".addData", function() {

        var ipLen = $(".custTbl").find("input");

        var dt = {}
        $(ipLen).each(function() {
            dt[$(this).attr("id")] = $(this).val();
        })

        jQuery(grid).jqGrid("addRowData", undefined, dt, "first");

        $(ipLen).each(function() {
            $(this).val('');
        })

        var grid_Obj = jQuery(grid).attr('id');
        var appEle = "#gbox_" + grid_Obj
        if (chkBoxShowHide == false) {


            $(appEle).find(".cbox").css("opacity", "0").attr("disabled", "disabled");
        }

    });

    if (addFrm) {
        var colLen = gridVarJson.colModel.length;
        var tblEle = $("<table class='table table-striped custom-table custTbl' />")
        var tbRow = $("<tr/>")
        var colGrp = $("<colgroup>")

        for (var i = 0; i < colLen; i++) {
            $(colGrp).append("<col style='width:" + gridVarJson.colModel[i].width + "%'>")
        }
        $(colGrp).prepend("<col style='width:25px'>")
        $(tblEle).append(colGrp)
        for (var i = 0; i < colLen; i++) {
            if (gridVarJson.colModel[i].name != 'edit' && gridVarJson.colModel[i].name != 'del') {
                $(tbRow).append("<td class='tbl-text'><input type='text' id='" + gridVarJson.colModel[i].name + "'/></td>")
            } else {
                $(tbRow).append("<td class='tbl-text " + gridVarJson.colModel[i].name + "'></td>")
            }
        }
        $(tbRow).prepend("<td>&nbsp;</td>")
        $(tbRow).find("td.edit").html("<span class='icn-add addData'> </span>")
        $(tblEle).append(tbRow);
        $(grid).before(tblEle);
    }

    var colModel1 = grid.jqGrid('getGridParam', 'colModel');
    var sortName = grid.jqGrid('getGridParam', 'sortname');

    $('#gbox_' + $.jgrid.jqID(grid[0].id) +
        ' tr.ui-jqgrid-labels th.ui-th-column').each(function(i) {

        var cmi = colModel1[i],
            colName = cmi.name;

        if (cmi.sortable !== false) {
            $(this).find('>div.ui-jqgrid-sortable>span.s-ico').show();
        } else if (!cmi.sortable && colName !== 'rn' && colName !== 'cb' && colName !== 'subgrid') {
            $(this).find('>div.ui-jqgrid-sortable').css({
                cursor: 'default'
            });
        }
        if (cmi.name === sortName) {
            $(this).addClass('sortedColumnHeader');
        }
    });

    applyClassesToHeaders(grid)
}

function resizeGrid(grid, wdthCont) {
    // jQuery(window).unbind('resize')
    jQuery(window).bind('resize', function() {

        //console.log($(grid).attr("id"))
        //console.log(jQuery('#stockInfoGrid').parents('.gridContainer').width());

        // Get width of parent container
        var width = jQuery(wdthCont).width();
        
        if (width == null || width < 1) {
            // For IE, revert to offsetWidth if necessary
            width = jQuery(wdthCont).width();
        }
        width = width - 2; // Fudge factor to prevent horizontal scrollbars
        if (width > 0 &&
            // Only resize if new width exceeds a minimal threshold
            // Fixes IE issue with in-place resizing when mousing-over frame bars
            Math.abs(width - jQuery(grid).width()) > 5) {
            jQuery(grid).setGridWidth(width);
            //console.log("sdgsddsf---------"+width)
            jQuery(grid).find(".ui-jqgrid-view").width(width + 2);
                 $(grid).parents(".ui-jqgrid-view").find(".ui-jqgrid-htable").width(width)
            
            //$(grid).find(".ui-jqgrid-hbox").addClass("try");
        }

        //jQuery(grid).find(".ui-jqgrid-view").width(width + 2);
        

    }).trigger('resize');
}
    


var gridVar = {
    dt: [
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 dec' 15", narration: "bvscbscbm Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 dec' 15", narration: "bvscbscbm Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 dec' 15", narration: "bvscbscbm Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 dec' 15", narration: "bvscbscbm Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" },
        { date: "1 nov' 15", narration: "Loreum Ipsum Dolar", chqNo: "80472AB", valDate: "1 nov' 15", widrawlAmt: "2,000", depositAmt: "0", clsBalance: "2,45,56,567" }

    ],
    colName: ['DATE', 'NARRATION', 'CHEQUE/REF NO.', 'VALUE DATE', 'WITHDRAWALS', 'DEPOSITS', 'CLOSING BALANCE'],
    colModel: [
        { name: 'date', width: 11, sortable: true, classes: "text-caps" },
        { name: 'narration', width: 16, sortable: true },
        { name: 'chqNo', width: 16, sortable: true, classes: "text-caps" },
        { name: 'valDate', width: 13, sortable: true, classes: "text-caps" },
        { name: 'widrawlAmt', width: 15, sortable: true, classes: "text-green align-text-right" },
        { name: 'depositAmt', width: 11, sortable: true, classes: "text-green align-text-right" },
        { name: 'clsBalance', width: 18, sortable: true, classes: "text-green align-text-right" }

    ]
}

var utilizeGridVar = {
    dt: [
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "09 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "09 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" },
        { disDate: "07 apr' 15", tAmt: "2,445,567", amtPaid: "1,445,567", amtOuts: "45,567", tExpDate: "25 MAY' 15", daysDue: "24" }

    ],
    colName: ['DISBURSAL DATE', 'TRANCHE AMT.', 'AMt. PAID', 'AMT. OUTSTANDING', 'TRANCHE EXPIRY DATE', 'DAYS PAST DUE'],
    colModel: [
        { name: 'disDate', width: 16, classes: "legend text-caps" },
        { name: 'tAmt', width: 17, sortable: true, classes: "text-green align-text-right" },
        { name: 'amtPaid', width: 13, sortable: true, classes: "text-green align-text-right" },
        { name: 'amtOuts', width: 20, sortable: true, classes: "text-green align-text-right" },
        { name: 'tExpDate', width: 21, sortable: true, classes: "text-caps" },
        { name: 'daysDue', width: 15, sortable: true }
    ]
}

var docGridVar = {
    dt: [
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" }

    ],
    colName: ['DOCUMENT NAME', 'DESCRIPTION', 'DUE FROM', 'DPD', 'REMARKS'],
    colModel: [
        { name: 'docName', width: 21 },
        { name: 'desc', width: 25, sortable: true },
        { name: 'dueFrom', width: 16, sortable: true, classes: "text-caps" },
        { name: 'dpd', width: 13, sortable: true },
        { name: 'REMARKS', width: 25, sortable: true, classes: "" }
    ]
}

var faciGridVar = {
    dt: [
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "valid", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" },
        { facType: "IF/TA", validType: "failed", accNo: "000067890", facAmt: "8,20,00,000", prinOs: "4,70,00,000", overDue: "45,00,000", othChrg: "56,00,00,000", tenLeft: "0" }

    ],
    colName: ['FACILITY TYPE', 'ACCOUNT NO.', 'FACILITY AMT.', 'PRINCIPLE O/S', 'OVERDUE', 'OTHER CHARGES', 'TENURE LEFT'],
    colModel: [
        { name: 'facType', width: 14, classes: "legend text-caps" },
        { name: 'accNo', width: 14, sortable: true },
        { name: 'facAmt', width: 14, sortable: true, classes: "text-green align-text-right" },
        { name: 'prinOs', width: 18, sortable: true, classes: "text-green align-text-right" },
        { name: 'overDue', width: 11, sortable: true, classes: "text-green align-text-right" },
        { name: 'othChrg', width: 16, sortable: true, classes: "text-green align-text-right" },
        { name: 'tenLeft', width: 15, sortable: true }
    ]
}

var grpSumGridVar = {
    dt: [
        { cifId: "000067890", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067893", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067894", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067895", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067896", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067891", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067892", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067893", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "678910000", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067898", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067899", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067891", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067892", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067892", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067893", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067894", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067895", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067896", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067899", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067891", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067892", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067892", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067893", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067894", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067895", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067896", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" },
        { cifId: "000067890", custName: "ADVAITH MOTORS PVT LTD-0000062174", loc: "Banglore", lmtAmt: "8,20,000,000", avlBal: "-3,456,789.98", clrFunds: "0", unClr: "0", utiz: "234,557,890.90", overdue: "Yes", docDue: "Yes" }

    ],
    colName: ['CIF ID', 'Customer Name', 'Location', 'LIMIT AMT. (IF/TA)', 'AVL. BALANCE',
        'CLEARED FUNDS', 'UNCLEAR', 'UTILIZATION', 'OVERDUES', 'DOCUMENTS DUE'
    ],
    colModel: [
        { name: 'cifId', width: 9 },
        { name: 'custName', width: 13, sortable: true, classes: "text-caps" },
        { name: 'loc', width: 9, sortable: true },
        { name: 'lmtAmt', width: 10, sortable: true, classes: "text-green align-text-right" },
        { name: 'avlBal', width: 11, sortable: true, classes: "text-green align-text-right" },
        { name: 'clrFunds', width: 8, sortable: true },
        { name: 'unClr', width: 8, sortable: true }, {
            name: 'utiz',
            width: 12,
            sortable: true,
            classes: "text-green align-text-right",
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a href="template_utilizationDetails.html" class="gridLink">' + cellvalue + '</a>';
            }
        }, {
            name: 'overdue',
            width: 10,
            sortable: true,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="yesNolink" href="template_overdueDetails.html">' + cellvalue + '</a>';
            }
        }, {
            name: 'docDue',
            width: 10,
            sortable: true,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="yesNolink" href="template_pendingDocuments.html">' + cellvalue + '</a>';
            }
        },
    ]
}

var utilzPopupGridVar = {
    dt: [
        { outstd: "9,531,425.32", curdateDis: "0", loc: "Banglore", interDue: "0", clrFund: "0", interDpd: "0", utiz: "0" }
    ],
    colName: ['Outstanding', 'WP/Current-date DISBURSEment', 'interest Due', 'Clear funds', 'interest DPD',
        'UTILIZATION'
    ],
    colModel: [
        { name: 'outstd', width: 14, sortable: false },
        { name: 'curdateDis', width: 30, sortable: false },
        { name: 'interDue', width: 14, sortable: false },
        { name: 'clrFund', width: 14, sortable: false },
        { name: 'interDpd', width: 14, sortable: false },
        { name: 'utiz', width: 14, sortable: false }
    ]
}

var incidentGridVar = {
    dt: [
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "0034563555", TranId: "2346789", edit: "Model Details", del: "" },
        { cifNo: "00000063555", amtDisbursed: "16,000,000", virtAcNo: "0034563555", dealercode: "7734563555", TranId: "2313789", edit: "Model Details", del: "" }

    ],
    colName: ['CIF NO.', 'AMT. TO BE DISBURSED', 'VIRTUAL A/C NO.', 'DEALER UNIQUE CODE', 'TRANCHE ID',
        'EDIT', ''
    ],
    colModel: [
        { name: 'cifNo', width: 12, sortable: false },
        { name: 'amtDisbursed', width: 20, sortable: false, classes: "text-green align-text-right" },
        { name: 'virtAcNo', width: 18, sortable: false },
        { name: 'dealercode', width: 20, sortable: false },
        { name: 'TranId', width: 13, sortable: false }, {
            name: 'edit',
            width: 12,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink " href="template_initiateIndent_modelDetails.html">' + cellvalue + '</a>';
            }
        }, {
            name: 'del',
            width: 5,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-del" href="#">' + cellvalue + '</a>';
            }
        },
    ]
}

var indentStatusGridVar = {
    dt: [
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "2", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "2", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "2", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "2", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "2", totVal: "12,000,000" },
        { modName: "Accent", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" },
        { modName: "Duster", modCode: "02345", valVahicle: "40,00,000", noVahicle: "3", totVal: "12,000,000" }

    ],
    colName: ['MODEL NAME', 'MODEL CODE', 'VALUE / VEHICLE', 'No. OF VEHICLES', 'TOTAL VALUE'],
    colModel: [
        { name: 'modName', width: 25, sortable: false },
        { name: 'modCode', width: 25, sortable: false },
        { name: 'valVahicle', width: 14, sortable: false, classes: "text-green align-text-right" },
        { name: 'noVahicle', width: 23, sortable: false },
        { name: 'totVal', width: 12, sortable: false, classes: "text-green align-text-right" }
    ]
}

var summaryGridVar = {
    dt: [
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "21,456,556", indDisbursed: "04", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "04" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "23,456,567", indDisbursed: "02", indPending: "02" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "21,456,556", indDisbursed: "04", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "04" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "23,456,567", indDisbursed: "02", indPending: "02" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "21,456,556", indDisbursed: "04", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "04" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "23,456,567", indDisbursed: "02", indPending: "02" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "21,456,556", indDisbursed: "04", indPending: "01" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "04" },
        { cifNo: "00000055456", indRaise: "06", valLakhs: "23,456,567", indDisbursed: "02", indPending: "02" },
        { cifNo: "00000063555", indRaise: "03", valLakhs: "23,456,567", indDisbursed: "06", indPending: "01" }

    ],
    colName: ['CIF NUMBER', 'INDENT  RAISED', 'VALUE (in lakhs)', 'INDENTS  DISBURSED', 'INDENTS  PENDING', 'Action'],
    colModel: [
        { name: 'cifNo', width: 20, sortable: false },
        { name: 'indRaise', width: 20, sortable: false },
        { name: 'valLakhs', width: 14, sortable: false, classes: "text-green align-text-right" },
        { name: 'indDisbursed', width: 18, sortable: false },
        { name: 'indPending', width: 18, sortable: false }, {
            name: '',
            width: 10,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a href="#" class="icn-cancel message-toggle" data-toggle="modal" data-target="#decline-indent-modal-sm">';
            }
        }
    ]
}

var detailsGridVar = {
    dt: [
        { cifNo: "00000063555", transNo: "00089765", indAmt: "400", initDateTime: "15 Dec '15,  4:35 pm", curStatus: "In-Progress", remarks: "View exception" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "600", initDateTime: "15 Dec '15,  7:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "5678", initDateTime: "16 Dec '15,  4:45 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "4334", initDateTime: "16 Dec '15,  4:23 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "1254", initDateTime: "17 Dec '15,  6:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "400", initDateTime: "15 Dec '15,  4:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "600", initDateTime: "15 Dec '15,  7:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "5678", initDateTime: "16 Dec '15,  4:45 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "4334", initDateTime: "16 Dec '15,  4:23 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "600", initDateTime: "15 Dec '15,  7:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "5678", initDateTime: "16 Dec '15,  4:45 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "4334", initDateTime: "16 Dec '15,  4:23 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "1254", initDateTime: "17 Dec '15,  6:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "400", initDateTime: "15 Dec '15,  4:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "600", initDateTime: "15 Dec '15,  7:35 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "5678", initDateTime: "16 Dec '15,  4:45 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000055456", transNo: "00089765", indAmt: "4334", initDateTime: "16 Dec '15,  4:23 pm", curStatus: "In-Progress", remarks: "" },
        { cifNo: "00000063555", transNo: "00089765", indAmt: "1254", initDateTime: "17 Dec '15,  6:35 pm", curStatus: "In-Progress", remarks: "" }

    ],
    colName: ['CIF NO.', 'TRANSACTION No.', 'INDENT AMT.', 'INITIATED DATE & TIME', 'CURRENT STATUS', 'REMARKS'],
    colModel: [
        { name: 'cifNo', width: 15, sortable: false }, {
            name: 'transNo',
            width: 18,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink" href="#">' + cellvalue + '</a>';
            }
        },
        { name: 'indAmt', width: 12, sortable: false, classes: "text-green align-text-right" },
        { name: 'initDateTime', width: 20, sortable: false },
        { name: 'curStatus', width: 17, sortable: false }, {
            name: 'remarks',
            width: 12,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a href="#" class="gridLink message-toggle" data-toggle="modal" data-target="#acc-bal-modal-sm">' + cellvalue + '</a>';
            }
        }
    ]
}

var uploadTempGridVar = {
    dt: [
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of English" },
        { docName: "Lorem Ipsum", docDesc: "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and  printing in place of Japanese" }
    ],
    colName: ['DOCUMENT NAME', 'DOCUMENT  DESCRIPTION'],
    colModel: [
        { name: 'docName', width: 18, sortable: true },
        { name: 'docDesc', width: 82, sortable: true }
    ]
}

/* Reports Page */

var loanReportGridVar = {
    dt: [
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
        { loanNo: "000067890", loanAmt: "2,45,56,567", loanBkDate: "1 nov' 15", loanMatDate: "1 nov' 15", balAmt: "2,45,56,567", intRate: "12", intAmt: "2,45,56,567" },
    ],
    colName: ['LOAN NO.', 'LOAN AMT.', 'LOAN BOOKING DT.', 'LOAN MATURITY Dt.', 'BALANCE AMT.', 'INTEREST  RATE', 'INTEREST AMT.'],
    colModel: [
        { name: 'loanNo', width: 10, sortable: false, editable: true },
        { name: 'loanAmt', width: 10, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'loanBkDate', width: 17, sortable: false, editable: true },
        { name: 'loanMatDate', width: 18, sortable: false, editable: true },
        { name: 'balAmt', width: 15, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'intRate', width: 15, sortable: false, editable: true },
        { name: 'intAmt', width: 15, sortable: false, editable: true, classes: "text-green align-text-right" },
    ]
}

var loanMonthlyGridVar = {
    dt: [
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
        { loanNo: "000067891", loanAmt: "2,45,22,347", loanBkDate: "12 nov' 15", loanMatDate: "21 nov' 15", balAmt: "2,45,56,522", intRate: "10", intAmt: "2,45,56,777" },
    ],
    colName: ['LOAN NO.', 'LOAN AMT.', 'LOAN BOOKING DT.', 'LOAN MATURITY Dt.', 'BALANCE AMT.', 'INTEREST  RATE', 'INTEREST AMT.'],
    colModel: [
        { name: 'loanNo', width: 10, sortable: false, editable: true },
        { name: 'loanAmt', width: 10, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'loanBkDate', width: 17, sortable: false, editable: true },
        { name: 'loanMatDate', width: 18, sortable: false, editable: true },
        { name: 'balAmt', width: 15, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'intRate', width: 15, sortable: false, editable: true },
        { name: 'intAmt', width: 15, sortable: false, editable: true, classes: "text-green align-text-right" },
    ]
}

var modelReportGridVar = {
    dt: [
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" }

    ],
    colName: ['MODEL NAME', 'STATUS', 'TOTAL COUNT', 'COUNT', 'INVOICE AMT.', 'COUNT', 'INVOICE AMT.', 'INVOICE AMT. TOTAL', 'INVOICE  INT'],
    colModel: [
        { name: 'modName', width: 12, sortable: false, classes: "text-caps" },
        { name: 'status', width: 8, sortable: false, classes: "text-caps" },
        { name: 'totCount', width: 12, sortable: false },
        { name: 'count', width: 7, sortable: false, classes: "border-left" },
        { name: 'invoAmt', width: 12, sortable: false, classes: "text-green align-text-right" },
        { name: 'count1', width: 7, sortable: false, classes: "border-left" },
        { name: 'invoAmt1', width: 12, sortable: false, classes: "text-green align-text-right" },
        { name: 'invoAmtTot', width: 18, sortable: false, classes: "border-left text-green align-text-right" },
        { name: 'invoInt', width: 12, sortable: false, classes: "text-green align-text-right" }
    ],
    subHeader: [
        { startColumnName: 'count', numberOfColumns: 2, titleText: 'WITHIN TRANCHE PERIOD', classes: "border-left" },
        { startColumnName: 'count1', numberOfColumns: 2, titleText: 'BEYOND TRANCHE PERIOD', classes: "border-left border-right" }
    ]
}

var loanModelReportGridVar = {
    dt: [
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" },
        { loanno: "0098765", modName: "ACCENT", status: "ON HOLD", totCount: "3456", count: "1 nov' 15", invoAmt: "2,45,56,567", count1: "4567", invoAmt1: "2,45,56,567", invoAmtTot: "2,45,56,567", invoInt: "5,86,567" }

    ],
    colName: ['loan no.', 'MODEL NAME', 'STATUS', 'TOTAL COUNT', 'COUNT', 'INVOICE AMT.', 'COUNT', 'INVOICE AMT.', 'INVOICE AMT. TOTAL', 'INVOICE  INT'],
    colModel: [
        { name: 'loanno', width: 8, sortable: false },
        { name: 'modName', width: 10, sortable: false, classes: "text-caps" },
        { name: 'status', width: 8, sortable: false, classes: "text-caps" },
        { name: 'totCount', width: 10, sortable: false },
        { name: 'count', width: 10, sortable: false, classes: "border-left" },
        { name: 'invoAmt', width: 10, sortable: false, classes: "text-green align-text-right" },
        { name: 'count1', width: 10, sortable: false, classes: "border-left" },
        { name: 'invoAmt1', width: 10, sortable: false, classes: "text-green align-text-right" },
        { name: 'invoAmtTot', width: 14, sortable: false, classes: "border-left text-green align-text-right" },
        { name: 'invoInt', width: 10, sortable: false, classes: "text-green align-text-right" }
    ],
    subHeader: [
        { startColumnName: 'count', numberOfColumns: 2, titleText: 'WITHIN TRANCHE PERIOD', classes: "border-left" },
        { startColumnName: 'count1', numberOfColumns: 2, titleText: 'BEYOND TRANCHE PERIOD', classes: "border-left border-right" }
    ]
}

var penDocReportGridVar = {
    dt: [
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" },
        { docName: "No consideration letter", desc: "Lorem ipsum dolor set amen", dueFrom: "25 MAY' 15", dpd: "34", REMARKS: "Lorem ipsum dolor set amen" }

    ],
    colName: ['DOCUMENT NAME', 'DESCRIPTION', 'DUE FROM', 'DPD', 'REMARKS'],
    colModel: [
        { name: 'docName', width: 21 },
        { name: 'desc', width: 25, sortable: true },
        { name: 'dueFrom', width: 16, sortable: true, classes: "text-caps" },
        { name: 'dpd', width: 13, sortable: true },
        { name: 'REMARKS', width: 25, sortable: true, classes: "" }
    ]
}

var retConvReportGridVar = {
    dt: [
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" },
        { monYr: "MAY’ 2015", benchMrkConv: "45,67,887", archRetConv: "45,67,887", excessShrtFall: "34", BankSrc: "45,78,000" }

    ],
    colName: ['MONTH - YEAR', 'BENCHMARK CONVERSION (     )', 'ACHIEVED RETAIL CONVERSION', 'EXCESS / SHORT FALL ( in %)', 'BANK SOURCING'],
    colModel: [
        { name: 'monYr', width: 12, sortable: false, classes: "text-caps" },
        { name: 'benchMrkConv', width: 25, sortable: false, classes: "text-green align-text-right" },
        { name: 'archRetConv', width: 25, sortable: false, classes: "text-green align-text-right" },
        { name: 'excessShrtFall', width: 25, sortable: false },
        { name: 'BankSrc', width: 13, sortable: false }
    ]
}

/* Submission Page */
var salesInfoGridVar = {
    dt: [
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" },
        { canNm1: "MAY’ 15 2323", canNm2: "456", canNm3: "12", canNm4: "3", canNm5: "20,000", canNm6: "345", canNm7: "178", oth: "234", totQty: "768", totAmt: "72.89  lakhs" }

    ],
    colName: ['CAR NAME 1', 'CAR NAME 2', 'CAR NAME 3', 'CAR NAME 4', 'CAR NAME 5', 'CAR NAME 6', 'CAR NAME 7', 'others', 'total qty.', 'total amt.'],
    colModel: [
        { name: 'canNm1', width: 11, sortable: false, classes: "text-caps" },
        { name: 'canNm2', width: 11, sortable: false },
        { name: 'canNm3', width: 11, sortable: false },
        { name: 'canNm4', width: 11, sortable: false },
        { name: 'canNm5', width: 11, sortable: false },
        { name: 'canNm6', width: 11, sortable: false },
        { name: 'canNm7', width: 11, sortable: false },
        { name: 'oth', width: 7, sortable: false },
        { name: 'totQty', width: 8, sortable: false },
        { name: 'totAmt', width: 10, sortable: false, classes: "text-green align-text-right" }
    ]
}


/* For Jqgrid rowspan */
var arrtSetting = function(rowId, val, rawObject, cm) {

    console.log(rawObject)
    var attr = rawObject.attr[cm.name],
        result;
    if (attr.rowspan) {
        result = ' rowspan=' + '"' + attr.rowspan + '"';
    } else if (attr.display) {
        result = ' style="display:' + attr.display + '"';
    }
    return result;
}

var stockInfoGridVar = {
    dt: [
        { bucket: "0 - 30 days" },
        { bucket: "31 - 60 days" },
        { bucket: "61 - 90 days" },
        { bucket: "90+ days" },
        { bucket: "Total" }

    ],
    colName: ['bucket'],
    colModel: [
        { name: 'bucket', sortable: false }
    ],
    subHeader: [
        { startColumnName: 'bucket', numberOfColumns: 1, titleText: 'STOCK  INFO' }
    ]
}

var stockInfoEmptyGrid1Var = {
    dt: [
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true }
    ],
    colName: ['stock', 'debters', 'creditors'],
    colModel: [
        { name: 'stock', width: 33, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' },
        { name: 'debters', width: 33, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' },
        { name: 'creditors', width: 34, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' }
    ],
    subHeader: [
        { startColumnName: 'stock', numberOfColumns: 3, titleText: 'VEHICLES (in lakhs)' }
    ]
}

var stockInfoEmptyGrid2Var = {
    dt: [
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true },
        { stock: "", debters: "", creditors: "", cellEditParam: true }
    ],
    colName: ['stock', 'debters', 'creditors'],
    colModel: [
        { name: 'stock', width: 33, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' },
        { name: 'debters', width: 33, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' },
        { name: 'creditors', width: 34, sortable: false, editable: true, edittype: 'text', summaryType: 'sum' }
    ],
    subHeader: [
        { startColumnName: 'stock', numberOfColumns: 3, titleText: 'SPARES (in lakhs)' }
    ]
}

var stockInfoVehiclesGridVar = {
    dt: [
        { stock: "2323", debters: "456", creditors: "432", cellEditParam: false },
        { stock: "254", debters: "498", creditors: "789", cellEditParam: false },
        { stock: "2323", debters: "456", creditors: "432", cellEditParam: false },
        { stock: "254", debters: "498", creditors: "789", cellEditParam: false }
    ],
    colName: ['stock', 'debters', 'creditors'],
    colModel: [
        { name: 'stock', width: 33, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' },
        { name: 'debters', width: 33, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' },
        { name: 'creditors', width: 34, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' }
    ],
    subHeader: [
        { startColumnName: 'stock', numberOfColumns: 3, titleText: 'VEHICLES (in lakhs)' }
    ]
}

var stockInfoSparesGridVar = {
    dt: [
        { stock: "2323", debters: "456", creditors: "432", cellEditParam: false },
        { stock: "254", debters: "498", creditors: "789", cellEditParam: false },
        { stock: "2323", debters: "456", creditors: "432", cellEditParam: false },
        { stock: "254", debters: "498", creditors: "789", cellEditParam: false }
    ],
    colName: ['stock', 'debters', 'creditors'],
    colModel: [
        { name: 'stock', width: 33, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' },
        { name: 'debters', width: 33, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' },
        { name: 'creditors', width: 34, sortable: false, editable: false, edittype: 'text', summaryType: 'sum' }
    ],
    subHeader: [
        { startColumnName: 'stock', numberOfColumns: 3, titleText: 'SPARES (in lakhs)' }
    ]
}

var stockFinalAuditGridVar = {
    dt: [
        { stockStus: "STOCK", bkt: "0 to 60", cnt: "400", valLakh: "23,567,990", perct: "23", finalVal: "45,567,990", attr: { stockStus: { rowspan: "4" } } },
        { stockStus: "STOCK", bkt: "60 to 90", cnt: "600", valLakh: "123,567,99", perct: "66", finalVal: "45,567,990", attr: { stockStus: { display: "none" } } },
        { stockStus: "STOCK", bkt: "Above 90", cnt: "400", valLakh: "23,567,990", perct: "23", finalVal: "45,567,990", attr: { stockStus: { display: "none" } } },
        { stockStus: "STOCK", bkt: "Stock (a)", cnt: "600", valLakh: "123,567,99", perct: "66", finalVal: "45,567,990", attr: { stockStus: { display: "none" } } },
        { stockStus: "RECEIVABLES", bkt: "0 to 30", cnt: "400", valLakh: "23,567,990", perct: "23", finalVal: "45,567,990", attr: { stockStus: { rowspan: "3" } } },
        { stockStus: "RECEIVABLES", bkt: "Above 30", cnt: "600", valLakh: "123,567,99", perct: "66", finalVal: "45,567,990", attr: { stockStus: { display: "none" } } },
        { stockStus: "RECEIVABLES", bkt: "Receivables (b)", cnt: "400", valLakh: "23,567,990", perct: "23", finalVal: "45,567,990", attr: { stockStus: { display: "none" } } },
        { stockStus: "TRANSIT", bkt: "Transit (c)", cnt: "600", valLakh: "123,567,99", perct: "66", finalVal: "45,567,990", attr: { stockStus: { rowspan: "1" } } }
    ],
    colName: ['STOCK STATUS', 'BUCKET', 'COUNT', 'VALUE (in lakhs)', '% CONSIDERED', 'FINAL VALUE'],
    colModel: [
        { name: 'stockStus', width: 25, sortable: false, classes: "text-caps align-text-center", cellattr: arrtSetting },
        { name: 'bkt', width: 15, sortable: false, classes: "border-left border-right" },
        { name: 'cnt', width: 15, sortable: false },
        { name: 'valLakh', width: 15, sortable: false, classes: "text-green align-text-right" },
        { name: 'perct', width: 15, sortable: false },
        { name: 'finalVal', width: 15, sortable: false, classes: "text-green align-text-right" }
    ]
}

var stockAuditGridVar = {
    dt: [
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "002", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "003", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "005", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "019", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "015", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "006", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "010", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "011", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" },
        { stockNo: "134680", invocNo: "001", invocDt: "22 May' 15", invocAmt: "1,445,567", invocFrm: "23 MAY' 15", dtofRecp: "25 MAY' 15", modlName: "24", engineNo: "234" }

    ],
    colName: ['SERIAL STOCK NO.', 'INVOICE NO.', 'INVOICE DATE', 'INVOICE AMT.', 'INVOICE FUND FROM', 'DATE OF RECIEPT', 'MODEL NAME', 'ENGINE NO.'],
    colModel: [
        { name: 'stockNo', width: 12.5, sortable: false, classes: "legend text-caps" },
        { name: 'invocNo', width: 12.5, sortable: false },
        { name: 'invocDt', width: 12.5, sortable: false, classes: "text-caps" },
        { name: 'invocAmt', width: 12.5, sortable: false, classes: "text-green align-text-right" },
        { name: 'invocFrm', width: 12.5, sortable: false, classes: "text-caps" },
        { name: 'dtofRecp', width: 12.5, sortable: false },
        { name: 'modlName', width: 12.5, sortable: false },
        { name: 'engineNo', width: 12.5, sortable: false }
    ]
}

var loanWiseStckInfoGridVar = {
    dt: [
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "Upload", stckStat: "Upload" }

    ],
    colName: ['LOAN NUMBER', 'LOAN DATE', 'LOAN AMOUNT', 'STOCK RECEIPT COUNT', 'STOCK RECEIPT VALUE', 'LOAN VS STOCK DIFFERENCE', 'VEHICLE SOLD', 'STOCK RECEIPT STATUS', 'STOCK SOLD STATUS', 'VEHICLES SOLD VALUE', 'STOCK RECEIPT', 'STOCK STATUS'],
    colModel: [
        { name: 'loanNo', width: 11, sortable: false },
        { name: 'loanDt', width: 8.5, sortable: false, classes: "text-caps" },
        { name: 'loanAmt', width: 9.6, sortable: false, classes: "text-green align-text-right" },
        { name: 'stckRecpCount', width: 7.6, sortable: false },
        { name: 'stckRecpVal', width: 7.6, sortable: false },
        { name: 'loanStckDiff', width: 10, sortable: false },
        { name: 'vehclSold', width: 8.3, sortable: false },
        { name: 'stckRecpStat', width: 8.3, sortable: false },
        { name: 'stckSoldStat', width: 8.3, sortable: false },
        { name: 'vehclSoldStat', width: 8.3, sortable: false }, {
            name: 'stckRecp',
            width: 8,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink" href="template_submission_loanWise_stockReceipt.html">' + cellvalue + '</a>';

            }
        }, {
            name: 'stckStat',
            width: 8,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink" href="template_submission_loanWise_stockStatus.html">' + cellvalue + '</a>';
            }
        },
    ]
}

var loanWiseStckInfoViewGridVar = {
    dt: [
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" },
        { loanNo: "240SL60142130145", loanDt: "22 MAY ‘ 15", loanAmt: "1,50,32,015", stckRecpCount: "0", stckRecpVal: "0", loanStckDiff: "3418610", vehclSold: "0", stckRecpStat: "Pending", stckSoldStat: "Pending", vehclSoldStat: "0", stckRecp: "View", stckStat: "View" }

    ],
    colName: ['LOAN NUMBER', 'LOAN DATE', 'LOAN AMOUNT', 'STOCK RECEIPT COUNT', 'STOCK RECEIPT VALUE', 'LOAN VS STOCK DIFFERENCE', 'VEHICLE SOLD', 'STOCK RECEIPT STATUS', 'STOCK SOLD STATUS', 'VEHICLES SOLD VALUE', 'STOCK RECEIPT', 'STOCK STATUS'],
    colModel: [
        { name: 'loanNo', width: 11, sortable: false },
        { name: 'loanDt', width: 8.5, sortable: false, classes: "text-caps" },
        { name: 'loanAmt', width: 9.6, sortable: false, classes: "text-green" },
        { name: 'stckRecpCount', width: 7.6, sortable: false },
        { name: 'stckRecpVal', width: 7.6, sortable: false },
        { name: 'loanStckDiff', width: 10, sortable: false },
        { name: 'vehclSold', width: 8.3, sortable: false },
        { name: 'stckRecpStat', width: 8.3, sortable: false },
        { name: 'stckSoldStat', width: 8.3, sortable: false },
        { name: 'vehclSoldStat', width: 8.3, sortable: false }, {
            name: 'stckRecp',
            width: 8,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink message-toggle" href="#" data-toggle="modal" data-target="#stckRecepGrid-modal-sm">' + cellvalue + '</a>';

            }
        }, {
            name: 'stckStat',
            width: 8,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="gridLink message-toggle" href="#" data-toggle="modal" data-target="#stckStatusGrid-modal-sm">' + cellvalue + '</a>';
            }
        },
    ]
}

var loanWiseStockStatusVar = {
    dt: [
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" },
        { vinNo: "1A234", payRecievDt: "02 JUNE 15", payValReciev: "213" }
    ],
    colName: ['VIN NUMBER', 'PAYMENT RECEIVED DATE', 'PAYMENT VALUE RECEIVED'],
    colModel: [
        { name: 'vinNo', width: 33.33, sortable: true },
        { name: 'payRecievDt', width: 33.33, sortable: true },
        { name: 'payValReciev', width: 33.33, sortable: true }
    ]
}

var loanWiseStockRecepVar = {
    dt: [
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" },
        { modlName: "KENYA", invocVal: "7", vinNo: "1A234", vehclRecDt: "02 JUNE 15" }
    ],
    colName: ['MODEL NAME', 'INVOICE VALUE', 'VIN NO', 'VEHICLE RECIEVED DATE'],
    colModel: [
        { name: 'modlName', width: 25, sortable: true },
        { name: 'invocVal', width: 25, sortable: true },
        { name: 'vinNo', width: 25, sortable: true },
        { name: 'vehclRecDt', width: 25, sortable: true }
    ]
}

var stckAuditinPrcssVar = {
    dt: [
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" },
        { stckAudtNo: "SAM14070567468", audtDt: "25 MAY ‘  15", decsion: "", stckDetl: "Upload" }

    ],
    colName: ['STOCK AUDIT NUMBER', 'AUDIT DATE', 'DECISION', 'STOCK DETAILS'],
    colModel: [
        { name: 'stckAudtNo', width: 25, sortable: false, classes: "text-caps" },
        { name: 'audtDt', width: 20, sortable: false, classes: "text-caps" }, {
            name: 'decsion',
            width: 35,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<select class="custum-selectBox light-grey"><option>Upload not done1</option><option>Upload not done2</option><option>Upload not done3</option></select>';
            }
        }, {
            name: 'stckDetl',
            width: 20,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<span class="icon-upload"></span><a class="gridLink upldLink" href="template_submission_stockAudit_upload.html">' + cellvalue + '</a>';
            }
        },
    ]
}

var stckAuditHistoryVar = {
    dt: [
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Review done- Proceed", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" },
        { stckAuditNo: "SAM14070567468", auditDt: "25 MAY'  15", decision: "Upload not done", status: "25 MAY'  15", viewTrail: "", updtStckDet: "", finalAudSumm: "", finalAudDetl: "" }

    ],
    colName: ['STOCK AUDIT NO.', 'AUDIT DATE', 'DECISION', 'STATUS', 'VIEW TRAILS', 'UPDATED STOCK DETAILS', 'FINAL AUDIT SUMM.', 'FINAL AUDIT DETAILS'],
    colModel: [
        { name: 'stckAuditNo', width: 14, sortable: false, classes: "text-caps" },
        { name: 'auditDt', width: 10, sortable: false },
        { name: 'decision', width: 12, sortable: false },
        { name: 'status', width: 10, sortable: false }, {
            name: 'viewTrail',
            width: 10,
            sortable: false,
            classes: "border-left",
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-eye" href="#" class="message-toggle" data-toggle="modal" data-target="#reqIndentGrid-modal-sm">' + cellvalue + '</a>';
            }
        }, {
            name: 'updtStckDet',
            width: 14,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-stock" href="#">' + cellvalue + '</a>';
            }
        }, {
            name: 'finalAudSumm',
            width: 12,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-notes" href="template_submissions_stockFinalAuditSummary.html">' + cellvalue + '</a>';
            }
        }, {
            name: 'finalAudDetl',
            width: 12,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-folder" href="template_submission_finalAudit_detail.html">' + cellvalue + '</a>';
            }
        },
    ]
}
var stckRecepGridVar = {
    dt: [
        { modelNam: "Kenya", invocVal: "07", vinNo: "1A234", recievDt: "02 June’ 15" },
        { modelNam: "Kenya", invocVal: "06", vinNo: "1A232", recievDt: "02 June’ 15" },
        { modelNam: "Kenya", invocVal: "05", vinNo: "1A230", recievDt: "02 June’ 15" }
    ],
    colName: ['MODEL NAME', 'INVOICE VALUE', 'VIN NO', 'VEHICLE RECIEVED DATE'],
    colModel: [
        { name: 'modelNam', width: 25, sortable: true, classes: "text-caps" },
        { name: 'invocVal', width: 25, sortable: true },
        { name: 'vinNo', width: 25, sortable: true, classes: "text-caps" },
        { name: 'recievDt', width: 25, sortable: true, classes: "align-text-right" }
    ]
}
var stckStatusGridVar = {
    dt: [
        { invocVal: "07", vinNo: "1A234", recievDt: "10,23,564" },
        { invocVal: "06", vinNo: "1A232", recievDt: "10,23,564" },
        { invocVal: "05", vinNo: "1A230", recievDt: "10,23,564" }
    ],
    colName: ['INVOICE VALUE', 'PAYMENT RECIEVED DATE', 'PAYMENT VALUE RECEIVED'],
    colModel: [
        { name: 'invocVal', width: 33.33, sortable: true },
        { name: 'vinNo', width: 33.33, sortable: true },
        { name: 'recievDt', width: 33.33, sortable: true, classes: "text-green" }
    ]
}

/* Grid data with json */
var modelDetGridVar = {
    colName: ['MODEL NAME', 'MODEL CODE', ' VALUE / VEHICLE', 'No. OF VEHICLES', 'MARGIN',
        ' TOTAL VALUE', 'CITY', 'REMARKS', 'Actions', ''
    ],
    colModel: [
        { name: 'modName', width: 10, sortable: false, editable: true },
        { name: 'modCode', width: 10, sortable: false, editable: true },
        { name: 'valVehi', width: 15, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'noVehi', width: 11, sortable: false, editable: true },
        { name: 'margin', width: 10, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'totVal', width: 10, sortable: false, editable: true, classes: "text-green align-text-right" },
        { name: 'city', width: 10, sortable: false, editable: true },
        { name: 'rem', width: 10, sortable: false, editable: true }, {
            name: 'edit',
            width: 8,
            sortable: false,
            classes: "align-text-right",
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-edit" href="#"></a>';
            }
        }, {
            name: 'del',
            width: 6,
            sortable: false,
            formatter: function(cellvalue, options, rowObject) {
                var cellPrefix = '';
                return cellPrefix + '<a class="icn-del" href="#">delete</a>';
            }
        },
    ]
}

function msgBlock(obj, msgBlk) {
    $(msgBlk).hide();
    $(obj).on('click', function() {
        $(msgBlk).fadeIn();
        setTimeout(function() {
            $(msgBlk).fadeOut();
        }, 5000);
    })
}

function loc_multiselect(obj, objCont) {
    $(obj).multipleSelect({
        placeholder: "Mumbai",
        width: '100%',
        filter: true,
        selTagContainer: ".cifno-tag-cont",
        onComplete: function() {
            selectTagContainer(obj, objCont)
        },
        onClick: function(view) {
            selectTagContainer(obj, objCont)
                /*--- update cifno on selection of  data ----*/
            var dummyData = [
                { "lbl": "opt1", "val": "opt1" },
                { "lbl": "opt2", "val": "opt2" },
                { "lbl": "opt3", "val": "opt3" },
                { "lbl": "opt4", "val": "opt4" }
            ]

            if (view.label == "Delhi") {
                if (view.checked) {
                    var opts = "";
                    $(dummyData).each(function(e) {
                        opts += "<option value='" + this.val + "' >" + this.lbl + "</option>"
                    })
                    $("#cifno-select").html(opts)
                    $('#cifno-select').multipleSelect('refresh');
                }

            }
            /*--- end update cifno on selection of  data ----*/
        },
        onCheckAll: function(view) {
            selectTagContainer(obj, objCont)
        },
        onUncheckAll: function(view) {
            selectTagContainer(obj, objCont)
        }
    })
}

function cifno_multiselect(obj, objCont) {
    $("#cifno-select").multipleSelect({
        placeholder: "BPRG Access oooo86o",
        width: '100%',
        filter: true,
        selTagContainer: ".cifno-tag-cont",
        onComplete: function() {
            selectTagContainer(obj, objCont)
        },
        onClick: function(view) {
            selectTagContainer(obj, objCont)
        },
        onCheckAll: function(view) {
            selectTagContainer(obj, objCont)
        },
        onUncheckAll: function(view) {
            selectTagContainer(obj, objCont)
        }
    })
}

function selectTagContainer(obj, objCont) {
    var tt = $(obj).multipleSelect('getSelects');
    var val = $(obj).multipleSelect('getSelects', "text");
    var selData = "";
    for (var i = 0; i < tt.length; i++) {
        selData += "<label data-id=" + tt[i] + ">" + val[i] + "<span class='close'></span></label>"
    }
    $(objCont).html(selData);
    $(objCont).on("click", ".close", function() {
        var selVal = [];
        $(this).parent().remove();
        $(objCont).find("label").each(function() {
            selVal.push($(this).attr("data-id"))
        })
        $(obj).multipleSelect('setSelects', selVal)
    })
}

function custom_selectlist(obj, lstCont, doneBtn) {

    $(lstCont).slideUp();
    $(obj).on("click", function() {
        $(this).each(function(e) {
            if ($(this).parent().hasClass('active') == true) {
                $(this).parent().removeClass('active')
                $(this).parent().find(lstCont).slideUp();
            } else {
                $(this).parent().addClass('active')
                $(this).parent().find(lstCont).slideDown();
            }
        })
    })
    $(doneBtn).on("click", function() {
        $(this).parents(lstCont).slideUp();
        $(obj).parent().removeClass("active")
    })
}

/*Document Ready*/
$(function() {

    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });

    //Checkbox 
    if ($(".checkbox").length != 0) {
        var chkbtnName = $(".checkbox input[type=checkbox]");
        chkBoxClick(chkbtnName);
    }

    //Checkbox 
    if ($(".select-list-cont").length != 0) {
        var chkbtnName = $(".select-list-cont input[type=checkbox]");
        chkBoxClick(chkbtnName);
    }

    // Virtual Keyboard functionality
    if ($("#virtualKeypad").length != 0) {
        keypadShowHide();
    }

    // Carousel Login Banner
    if ($("#banner-slider").length != 0) {
        loginBanner();
    }

    // slide up and down box
    if ($(".message-wrap").length != 0) {
        slideUpDown(".message-wrap", ".tab-wrap", ".message-toggle");
    }

    if ($(".field-details").length != 0) {
        showHide(".summary-title", ".field-details", ".summary-wrap");
    }

    if ($(".table-summary").length != 0) {
        showHide(".btnGo", ".table-summary", ".table-details");
    }

    if ($("#theGrid").length != 0) {
        var defGrid = jQuery("#theGrid")
        createGrid(defGrid, gridVar.dt, gridVar.colName, gridVar.colModel, false, '#theGridPager')
        resizeGrid(theGrid, ".gridContainer");
    }

    if ($("#utilizeGrid").length != 0) {
        var utiGrid = jQuery("#utilizeGrid")
        createGrid(utiGrid, utilizeGridVar.dt, utilizeGridVar.colName, utilizeGridVar.colModel, false, '#utilizeGridPager')
        resizeGrid(utiGrid, ".gridContainer");
    }

    if ($("#docGrid").length != 0) {
        var docGrid = jQuery("#docGrid")
        createGrid(docGrid, docGridVar.dt, docGridVar.colName, docGridVar.colModel, false, '#docGridPager')
        resizeGrid(docGrid, ".gridContainer");
    }

    if ($("#faciGrid").length != 0) {
        var faciGrid = jQuery("#faciGrid")
        createGrid(faciGrid, faciGridVar.dt, faciGridVar.colName, faciGridVar.colModel, false, '#faciGridPager')
        resizeGrid(faciGrid, ".gridContainer");
    }

    if ($("#grpSumGrid").length != 0) {
        var grpSumGrid = jQuery("#grpSumGrid")
        createGrid(grpSumGrid, grpSumGridVar.dt, grpSumGridVar.colName, grpSumGridVar.colModel, false, '#grpSumPager')
        resizeGrid(grpSumGrid, ".gridContainer");
    }

    $('#grid-modal-sm').on('shown.bs.modal', function(e) {
        if ($("#utilzPopupGrid").length != 0) {
            var utilzPopupGrid = jQuery("#utilzPopupGrid")
            createGrid(utilzPopupGrid, utilzPopupGridVar.dt, utilzPopupGridVar.colName, utilzPopupGridVar.colModel, false, '')
            resizeGrid(utilzPopupGrid, "#grid-modal-sm .gridContainer");
        }
    })

    if ($("#incidentGrid").length != 0) {
        var incidentGrid = jQuery("#incidentGrid")
        createGrid(incidentGrid, incidentGridVar.dt, incidentGridVar.colName, incidentGridVar.colModel, true, '#incidentGridPager')
        resizeGrid(incidentGrid, ".gridContainer");
    }

    if ($("#indentStatusGrid").length != 0) {
        var indentStatusGrid = jQuery("#indentStatusGrid")
        createGrid(indentStatusGrid, indentStatusGridVar.dt, indentStatusGridVar.colName, indentStatusGridVar.colModel, false, '#indentStatusPager')
        resizeGrid(indentStatusGrid, ".gridContainer");
    }

    if ($("#summaryGrid").length != 0) {
        var summaryGrid = jQuery("#summaryGrid")
        createGrid(summaryGrid, summaryGridVar.dt, summaryGridVar.colName, summaryGridVar.colModel, false, '#summaryGridPager')
        resizeGrid(summaryGrid, ".gridContainer");
    }

    if ($("#detailsGrid").length != 0) {
        var detailsGrid = jQuery("#detailsGrid")
        createGrid(detailsGrid, detailsGridVar.dt, detailsGridVar.colName, detailsGridVar.colModel, false, '#detailsGridPager')
        resizeGrid(detailsGrid, ".gridContainer");
    }

    if ($("#uploadTempGrid").length != 0) {
        var uploadTempGrid = jQuery("#uploadTempGrid")
        createGrid(uploadTempGrid, uploadTempGridVar.dt, uploadTempGridVar.colName, uploadTempGridVar.colModel, true, '#uploadTempPager')
        resizeGrid(uploadTempGrid, ".gridContainer");
    }

    if ($("#loanReportGrid").length != 0) {
        var loanReportGrid = jQuery("#loanReportGrid")
        createGrid(loanReportGrid, loanReportGridVar.dt, loanReportGridVar.colName, loanReportGridVar.colModel, false, '#loanReportGridPager')
        resizeGrid(loanReportGrid, ".gridContainer");
    }

    if ($("#loanMonthlyGrid").length != 0) {
        var loanMonthlyGrid = jQuery("#loanMonthlyGrid")
        createGrid(loanMonthlyGrid, loanMonthlyGridVar.dt, loanMonthlyGridVar.colName, loanMonthlyGridVar.colModel, false, '#loanMonthlyGridPager')
        resizeGrid(loanMonthlyGrid, ".gridContainer");
    }

    if ($("#modelReportGrid").length != 0) {
        var modelReportGrid = jQuery("#modelReportGrid")
        createGrid(modelReportGrid, modelReportGridVar.dt, modelReportGridVar.colName, modelReportGridVar.colModel, false, '#modelReportGridPager', true, modelReportGridVar.subHeader)
        resizeGrid(modelReportGrid, ".gridContainer");
    }

    if ($("#loanModelReportGrid").length != 0) {
        var loanModelReportGrid = jQuery("#loanModelReportGrid")
        createGrid(loanModelReportGrid, loanModelReportGridVar.dt, loanModelReportGridVar.colName, loanModelReportGridVar.colModel, false, '#loanModelReportGridPager', true, loanModelReportGridVar.subHeader)
        resizeGrid(loanModelReportGrid, ".gridContainer");
    }

    if ($("#penDocReportGrid").length != 0) {
        var penDocReportGrid = jQuery("#penDocReportGrid")
        createGrid(penDocReportGrid, penDocReportGridVar.dt, penDocReportGridVar.colName, penDocReportGridVar.colModel, false, '#penDocReportGridPager')
        resizeGrid(penDocReportGrid, ".gridContainer");
    }

    if ($("#retConvReportGrid").length != 0) {
        var retConvReportGrid = jQuery("#retConvReportGrid")
        createGrid(retConvReportGrid, retConvReportGridVar.dt, retConvReportGridVar.colName, retConvReportGridVar.colModel, false, '#retConvReportGridPager')
        resizeGrid(retConvReportGrid, ".gridContainer");
    }

    if ($("#salesInfoGrid").length != 0) {
        var salesInfoGrid = jQuery("#salesInfoGrid")
        createGrid(salesInfoGrid, salesInfoGridVar.dt, salesInfoGridVar.colName, salesInfoGridVar.colModel, false, '#salesInfoGridPager', '', '', salesInfoGridVar)
        resizeGrid(salesInfoGrid, ".gridContainer");
    }

    if ($("#stockInfoGrid").length != 0) {
        var stockInfoGrid = jQuery("#stockInfoGrid")
        createGrid(stockInfoGrid, stockInfoGridVar.dt, stockInfoGridVar.colName, stockInfoGridVar.colModel, false, '', true, stockInfoGridVar.subHeader)
        resizeGrid(stockInfoGrid, jQuery("#stockInfoGrid").parents(".gridContainer"));
    }

    if ($("#stockInfoEmptyGrid1").length != 0) {
        var stockInfoEmptyGrid1 = jQuery("#stockInfoEmptyGrid1")
        createGrid(stockInfoEmptyGrid1, stockInfoEmptyGrid1Var.dt, stockInfoEmptyGrid1Var.colName, stockInfoEmptyGrid1Var.colModel, false, '', true, stockInfoEmptyGrid1Var.subHeader, stockInfoEmptyGrid1Var, true, true, true)
        resizeGrid(stockInfoEmptyGrid1, jQuery("#stockInfoEmptyGrid1").parents(".gridContainer"));
    }

    if ($("#stockInfoEmptyGrid2").length != 0) {
        var stockInfoEmptyGrid2 = jQuery("#stockInfoEmptyGrid2")
        createGrid(stockInfoEmptyGrid2, stockInfoEmptyGrid2Var.dt, stockInfoEmptyGrid2Var.colName, stockInfoEmptyGrid2Var.colModel, false, '', true, stockInfoEmptyGrid2Var.subHeader, stockInfoEmptyGrid2Var, true, true, true)
        resizeGrid(stockInfoEmptyGrid2, jQuery("#stockInfoEmptyGrid2").parents(".gridContainer"));
    }

    if ($("#stockInfoVehiclesGrid").length != 0) {
        var stockInfoVehiclesGrid = jQuery("#stockInfoVehiclesGrid")
        createGrid(stockInfoVehiclesGrid, stockInfoVehiclesGridVar.dt, stockInfoVehiclesGridVar.colName, stockInfoVehiclesGridVar.colModel, false, '', true, stockInfoVehiclesGridVar.subHeader, stockInfoVehiclesGridVar, true, true, true)
        resizeGrid(stockInfoVehiclesGrid, jQuery("#stockInfoVehiclesGrid").parents(".gridContainer"));
    }

    if ($("#stockInfoSparesGrid").length != 0) {
        var stockInfoSparesGrid = jQuery("#stockInfoSparesGrid")
        createGrid(stockInfoSparesGrid, stockInfoSparesGridVar.dt, stockInfoSparesGridVar.colName, stockInfoSparesGridVar.colModel, false, '', true, stockInfoSparesGridVar.subHeader, stockInfoSparesGridVar, true, true, true)
        resizeGrid(stockInfoSparesGrid, jQuery("#stockInfoSparesGrid").parents(".gridContainer"));
    }

    if ($("#stockFinalAuditGrid").length != 0) {
        var stockFinalAuditGrid = jQuery("#stockFinalAuditGrid")
        createGrid(stockFinalAuditGrid, stockFinalAuditGridVar.dt, stockFinalAuditGridVar.colName, stockFinalAuditGridVar.colModel, false, '')
        resizeGrid(stockFinalAuditGrid, ".gridContainer");
    }

    if ($("#loanWiseStockStatus").length != 0) {
        var loanWiseStockStatus = jQuery("#loanWiseStockStatus")
        createGrid(loanWiseStockStatus, loanWiseStockStatusVar.dt, loanWiseStockStatusVar.colName, loanWiseStockStatusVar.colModel, true, '#loanWiseStockStatusPager')
        resizeGrid(loanWiseStockStatus, ".gridContainer");
    }

    if ($("#loanWiseStockRecep").length != 0) {
        var loanWiseStockRecep = jQuery("#loanWiseStockRecep")
        createGrid(loanWiseStockRecep, loanWiseStockRecepVar.dt, loanWiseStockRecepVar.colName, loanWiseStockRecepVar.colModel, true, '#loanWiseStockRecepPager')
        resizeGrid(loanWiseStockRecep, ".gridContainer");
    }

    if ($("#stckAuditinPrcss").length != 0) {
        var stckAuditinPrcss = jQuery("#stckAuditinPrcss")
        createGrid(stckAuditinPrcss, stckAuditinPrcssVar.dt, stckAuditinPrcssVar.colName, stckAuditinPrcssVar.colModel, true, '#stckAuditinPrcssPager')
        resizeGrid(stckAuditinPrcss, ".gridContainer");
        $(".custum-selectBox.light-grey").select2();

    }

    if ($("#stckAuditHistory").length != 0) {
        var stckAuditHistory = jQuery("#stckAuditHistory")
        createGrid(stckAuditHistory, stckAuditHistoryVar.dt, stckAuditHistoryVar.colName, stckAuditHistoryVar.colModel, false, '#stckAuditHistoryPager')
        resizeGrid(stckAuditHistory, ".gridContainer");

    }

    if ($("#stockAuditGrid").length != 0) {
        var stockAuditGrid = jQuery("#stockAuditGrid")
        createGrid(stockAuditGrid, stockAuditGridVar.dt, stockAuditGridVar.colName, stockAuditGridVar.colModel, false, '#stockAuditGridPager')
        resizeGrid(stockAuditGrid, ".gridContainer");
    }

    if ($("#loanWiseStckInfoViewGrid").length != 0) {
        var loanWiseStckInfoViewGrid = jQuery("#loanWiseStckInfoViewGrid")
        createGrid(loanWiseStckInfoViewGrid, loanWiseStckInfoViewGridVar.dt, loanWiseStckInfoViewGridVar.colName, loanWiseStckInfoViewGridVar.colModel, false, '#loanWiseStckInfoViewGridPager')
        resizeGrid(loanWiseStckInfoViewGrid, ".gridContainer");
    }
    if ($("#loanWiseStckInfoGrid").length != 0) {
        var loanWiseStckInfoGrid = jQuery("#loanWiseStckInfoGrid")
        createGrid(loanWiseStckInfoGrid, loanWiseStckInfoGridVar.dt, loanWiseStckInfoGridVar.colName, loanWiseStckInfoGridVar.colModel, false, '#loanWiseStckInfoGridPager')
        resizeGrid(loanWiseStckInfoGrid, ".gridContainer");
    }

    $('#stckRecepGrid-modal-sm').on('shown.bs.modal', function(e) {
        if ($("#stckRecepGrid").length != 0) {
            var stckRecepGrid = jQuery("#stckRecepGrid")
            createGrid(stckRecepGrid, stckRecepGridVar.dt, stckRecepGridVar.colName, stckRecepGridVar.colModel, false, '')
            resizeGrid(stckRecepGrid, "#stckRecepGrid-modal-sm .gridContainer");
        }
    })
    $('#stckStatusGrid-modal-sm').on('shown.bs.modal', function(e) {
        if ($("#stckStatusGrid").length != 0) {
            var stckStatusGrid = jQuery("#stckStatusGrid")
            createGrid(stckStatusGrid, stckStatusGridVar.dt, stckStatusGridVar.colName, stckStatusGridVar.colModel, false, '')
            resizeGrid(stckStatusGrid, "#stckStatusGrid-modal-sm .gridContainer");
        }
    })

    if ($("#modelDetGrid").length != 0) {
        var modelDetGrid = jQuery("#modelDetGrid")
        createJsonGrid('data/model.json', modelDetGrid, modelDetGridVar.colName, modelDetGridVar.colModel, true, '#modelDetGridPager', modelDetGridVar, true, false)
        resizeGrid(modelDetGrid, ".gridContainer");
    }

    // summary Datepicker Js //
    if ($("#startDate").length != 0) {
        var startOpt = {
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-left"
        }
        var endOpt = {
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-left"
        }
        dateRange('#startDate', '#endDate', startOpt, endOpt);
    }

    if ($("#startDate1").length != 0) {
        var startOpt = {
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-right"
        }
        var endOpt = {
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-right"
        }
        dateRange('#startDate1', '#endDate1', startOpt, endOpt);
    }


    $(".tabScroll").mCustomScrollbar({
        axis: "y" // vertical scrollbar
    });

    $(".message-carousel-wrap .carousel-desc").mCustomScrollbar({
        axis: "y" // vertical scrollbar
    });

    $('#message-carousel').carousel({
        interval: false
    });

    customSelect();

    if ($(".instant-msg").length != 0) {
        msgBlock('.btn-save', '.model-det-msg');
    }

    if ($("#uploadBtn").length != 0) {
        $("#uploadBtn").on('change', function() {
            var path = $("#uploadBtn").val();
            console.log("path : " + path)
        })
    }

    $(".gridContainer .icn-stock").on("click", function() {
        var tt = $("<input type='file' />");
        tt.trigger("click");
        return false;
    })

    if ($(".tandcChkBox").length != 0) {
        custom_tandc()
    }

    if ($("#loc-select").length != 0) {
        loc_multiselect("#loc-select", ".loc-tag-cont");
    }

    if ($("#cifno-select").length != 0) {
        cifno_multiselect("#cifno-select", ".cifno-tag-cont");
    }

    if ($(".txt-link").length != 0) {
        custom_selectlist(".txt-link", ".select-listCont", ".select-listWrap .btn");
    }

});
